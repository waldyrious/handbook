---
title: "Key Reviews"
aliases:
- /handbook/key-review/
---

## Purpose

At most companies, this would be a quarterly meeting for senior function leaders to present priorities, progress, and risk mitigations to the CEO. We allow some additional stakeholders to attend to invite a broader range of perspectives, give visibility to peers across functions, and create broader accountability.

Members of [E-Group](/handbook/company/structure/#executives) and department leaders nominated by their E-Group leader can be required or optional. Their E-Group member will determine desired participation from within their function.

These meetings are designed to ensure that the CEO and function are aligned on:

1. Quarter-to-quarter variances.
1. Performance against the plan, forecast, and operating model.
1. Connection between OKRs and KPIs.
1. Progress against Yearlies and OKRs.
1. Accountability to the rest of the company.

## Key Metrics

1. [KPIs](/handbook/company/kpis/) of that function or department.
1. [OKRs](/handbook/company/okrs/) for that function.

## Timing

Key Reviews are once per quarter.

## Scheduling

The Staff EBA to the CFO is the scheduling DRI for the Key Reviews and will schedule a four hour block during the week after [Sales QBRs](../sales/qbrs/index.md).
They will break up the block into 8 blocks of 25 minutes each (one for each function) and invite relevant attendees to each session. 

The Key Review meeting invitations will utilize the CFO's Zoom account. The functional DRI and corresponding EBA will be set as co-hosts. The Key Reviews should always be recorded, with the functional DRI's EBA as the recording DRI.

A Key Review should not be cancelled without permission. Permission to cancel or make changes to the Key Review schedule must be requested in the [#key-review](https://gitlab.slack.com/archives/CPQT0TRFX) Slack channel, tagging the CFO for approval and the Staff EBA to the CFO for awareness. If the CFO is not available, the CEO will make the decision. All changes to the Key Review schedule and/or invites have to be posted in the [#key-review](https://gitlab.slack.com/archives/CPQT0TRFX) Slack channel.

## Invitees

Required invitees are members of E-Group, the CoS to the CEO, the EBA to the CFO, and the EBAs to the CEO.
Optional attendees are other team members as designated by their E-Group leader.

Key Review Meetings may contain MNPI. As materials are limited access, participation is limited. Key Review agendas and recordings should not be shared with anyone not on the invite. If the Key Review Meeting contains MNPI, verify everyone on the Zoom call is permitted to be exposed to that MNPI and ask those who are not permitted to disconnect - do not share MNPI until confirming that everyone in the meeting should have access to what is being discussed. Key Review meetings that contain MNPI must not be privately livestreamed to GitLab Unfiltered.

If the Key Review meeting will not contain MNPI, it can be privately livestreamed to GitLab Unfiltered.

Functions that have these quarterly meetings are:

1. Sales (Chris Weber - function DRI)
1. Marketing & Strategy (Ashley Kramer - function DRI)
1. People Group (Rob Allen - function DRI)
1. Finance (Brian Robins - function DRI)
1. Product (David DeSanto - function DRI)
1. Legal and Corporate Affairs (Robin Schulman - function DRI)
1. Security (Josh Lemos - function DRI)
1. Engineering (Sabrina Farmer - function DRI)

Function DRIs are expected to review the invite list in advance of each Key Review. If cross-functional topics are being discussed, they are encouraged to invite folks who should be involved in the conversation. If there are MNPI concerns, they may choose to link to a separate agenda for this part of the discussion.

If you would like to be added to a function's Key Review, ask your functional E-Group member to ask the Staff EBA to the CFO to add you.

## Meeting Format

Meetings can leverages the [KPI Slides project](https://gitlab.com/gitlab-com/kpi-slides).

Content to cover in each Key Review:

1. Attainment against Yearlies/OKRs from the previous quarter. Should include status, learnings, next steps, and any existing dependencies.
1. Plans for Yearlies/OKRs for the current quarter. Should include status, link to more details (OKRs in GitLab), and any risks/dependencies.
1. Review of what is working, what is not working, key actions being taken, and what (if any) support is needed. This should include:
        1. Results and learnings
        1. Actions being taken now, expected impact, and measurement plan
        1. (Optional) A highlight of one or two specific discussion topics to discuss or one decision to get alignment on. This helps to focus the conversation on important topics. Any highlighted topics should prioritize topics that need awareness or require coordination with/support from  other teams. This should be a good forum to highlight when things are off track.
        
1. Key priorities and objectives for the function within the coming quarter. Should include any concrete goals that should be revisited in the next Key Review.
1. Key Metrics Update.

Important notes:

1. Key Reviews are the primary forum for synchronously getting alignment and dependency commitments on function-level OKRs.
1. Before every Key Review, Key Review leads should update OKRs to capture progress from the previous quarter [Maintaining the status of OKRs](/handbook/company/okrs/#maintaining-the-status-of-okrs). As the Key Reviews often falls deep into the first month of the quarter, this update can include updates up until the date of the meeting.
1. A document will be linked from the calendar invite for participants to log questions or comments for discussion and to any additional track decisions & action items.
1. Every department KPI should be covered. This can be in the presentation and/or links to the handbook.
1. Wherever possible, the KPI or KR being reviewed should be compared to Plan, Target, or industry benchmark.
1. There is no presentation; the meeting is purely Q&A. Of course, people can ask to talk them through a slide. If you want to present, please [post a YouTube video](/handbook/marketing/marketing-operations/youtube/) and link that from the slide deck, agenda, and/or Slack.
1. The functional owner or their EBA is responsible for updating and posting their Key Review agenda 5 business days in advance of the meeting, post in the [#key-review](https://gitlab.slack.com/archives/CPQT0TRFX) Slack channel.
1. Video is not required, but if there is a video, it should be posted 72 hours in advance in the [#key-review](https://gitlab.slack.com/archives/CPQT0TRFX) Slack channel.
1. The agenda document should include a presentation link that is accessible to all team members on the invite, as well as a Q&A section.
1. If the Key Review does not contain MNPI or [not public](/handbook/communication/confidentiality-levels/#not-public) information, it should be privately streamed to YouTube Unfiltered in alignment with GitLab's [value of transparency](/handbook/values/#transparency). If the Key Review contains MNPI, it should not be streamed, but can be recorded and shared in a [limited access](/handbook/communication/confidentiality-levels/#limited-access) manner by including the link in the restricted Key Review agenda doc.
1. Should private streaming be unavailable, the recording should be uploaded to the GitLab Unfiltered [Key Reviews Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrlBfo0IBmV0BwJcPN59swi) on YouTube if it does not contain MNPI or [not public](/handbook/communication/confidentiality-levels/#not-public) information. It is the DRI's or their EBA's responsibility to ensure that the video, if not containing MNPI or [not public](/handbook/communication/confidentiality-levels/#not-public) information, is uploaded to GitLab Unfiltered with the correct visibility (private or public). If it contains MNPI, then the recording can be shared in a [limited access](/handbook/communication/confidentiality-levels/#limited-access) manner.
1. If the function DRI is not available to attend their Key Review, the function DRI can designate a person or people from their leadership team to provide updates, share views, and cascade information.
1. Repetition in recognizing what is working or not working each Key Review is okay as repetition can be an organizational tool for teaching people what's important amidst all the noise.

Also see [First Post is a badge of honor](/handbook/communication/#first-post-is-a-badge-of-honor).

### Group Conversations and Key Review Metrics

Function DRIs are expected to use and share their function's Key Review deck, meeting notes and recordings for their scheduled [Group Conversations](group-conversations.md#current-schedule). Any MNPI in the Key Review decks should be removed. Async Group Conversations should follow Key Reviews. The goal for these meetings are to discuss what is important and the decks should be the same. You can add additional slides to the Key Review to give more context, not all slides have to relate to KPIs and OKRs. The difference between the two meetings is the audience, the GCs are for a wider audience of the entire company.

### Recordings

To view a recording of a Key Review, visit the playlist on YouTube Unfiltered titled [Key Reviews](https://www.youtube.com/playlist?list=PL05JrBw4t0KrlBfo0IBmV0BwJcPN59swi). If you are unable to view a video, please ensure you are logged in as [GitLab Unfiltered](/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube).

### Automated KPI Slides

The [original demo and proposal of using the KPI Slide project](https://youtu.be/5BlFNhSfS8A) is on GitLab Unfiltered (internal).

The [KPI Slides project](https://gitlab.com/gitlab-com/kpi-slides) is a GitLab-internal project since it includes KPIs that are [not public](/handbook/communication/confidentiality-levels/#not-public).
Those slides are deployed to GitLab pages (also internal, you must have access to the project to see the website).
Each group that presents has one markdown file with their KPIs.
Every month, groups create an MR to update that markdown file.
The following slides need to be updated with an MR:

- Month on the first slide
- Key Business Takeaways- should especially highlight any KPIs that need attention
- OKR statuses

The following Key Reviews are automated: (all links are internal)

- [Finance](https://gitlab-com.gitlab.io/kpi-slides/slides-html/finance/#/)
- [Infrastructure](https://gitlab-com.gitlab.io/kpi-slides/slides-html/infrastructure/#/)
- [Marketing & Strategy](https://gitlab-com.gitlab.io/kpi-slides/slides-html/marketing/#/)
- [People](https://gitlab-com.gitlab.io/kpi-slides/slides-html/people/#/)
- [Product](https://gitlab-com.gitlab.io/kpi-slides/slides-html/product/#/)

### Google Slides

1. The functional owner will prepare a google slide presentation with the content to be reviewed.
1. The finance business partner assigned to the functional area will meet with the owner at least one week in advance and ensure that follow-ups from last meeting have been completed and that data to be presented has proper definitions and is derived from a Single Source of Truth.
1. The finance business partner / preparer of the key review deck will ensure that the google slide permissions are set to comment for all in the GitLab domain.
1. The title of every slide should be the key takeaway
1. A label on the slide should convey whether the metric result is "on-track" (green), "needs improvement" (yellow), or is an "urgent concern" (red).
1. Teams are encouraged to use this [template](https://docs.google.com/presentation/d/1cdo138YJcFl_x9-vaybRJgcO_eSiLnOGw9b-JZidX3A/edit?usp=sharing).
1. Presentations should allow commenting from everyone at GitLab.

### Leveraging Sisense in Google Slides

To create these slides, you will need *Editor* access in Sisense.

- Edit the chart you're interested in.
- Click "Chart Format" (second tab on far right)
- Under Advanced, select "Expose Public CSV URL"
- Follow the [instructions on pulling data out of Sisense](/handbook/business-technology/data-team/platform/periscope/#pulling-data-out-of-sisense)
- Select the data set and add a chart to the sheet
- In the slides, Insert > Chart > From Sheets > find the one you're looking for

[Video with explanation of this meeting format](https://www.youtube.com/watch?v=plwfGXR9pjw&feature=youtu.be) (GitLab Internal)

### Performance Indicator Pages

Many functions or departments now have Performance Indicator pages that allow one to move top-to-bottom in the handbook to see both KPIs and PIs.
Here is an example of the VP of Development presenting the [Development Department's KPIs and PIs](/handbook/engineering/development/performance-indicators/) in advance of their monthly Key Review.

{{< youtube "8C9oniGinYM" >}}

This method is ideal, as it is [handbook-first](/handbook/about/handbook-usage/#why-handbook-first) and makes it easy for [everyone to contribute](strategy/).
Commentary can be added in an MR to the [data/performance_indicators.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators.yml) file.
The executive summary section can help consumers of the information understand where to dig in further.

The difficulty in using performance indicator pages for Key Reviews is for groups who have a significant number of Performance Indicators that [cannot be public](/handbook/communication/confidentiality-levels/#not-public).
For folks looking to consume this information quickly, having to 2FA into Sisense to see the information can be frustrating.
For functions or departments for which this is true, it is recommended to use a different Key Review format.

### OKR Slides

The Key Review should address new OKRs **and** an update on [scoring](okrs/_index.md#scoring-okrs) of what we have achieved in the previous quarter. This can include updates in the first quarter leading up to the time of the Key Review. Slides should:

1. Attainment against function level OKRs from the previous quarter, including:
     1. Link to OKRs in GitLab
     1. [DRI](/handbook/people-group/directly-responsible-individuals/)
     1. Completion [score](okrs/_index.md#scoring-okrs)
     1. Update summary
     1. Learnings
     1. Remaining dependencies to achievement
     1. Next steps, if incomplete

1. Plans for function level OKRs for the current quarter, including:
     1. Link to OKRs in GitLab
     1. [DRI](/handbook/people-group/directly-responsible-individuals/)
     1. Score (work may have already started in quarter)
     1. Health status (at risk, needs attention, on track)
     1. Status
     1. Risks and dependencies

At the end of each meeting, all attendees should be able to clearly answer what we are trying to achieve and whether we are on track.
