---
title: Laptop Buyback
---

## Overview

If the team member has not completed 1 calendar year at the time of offboarding or has received a laptop refresh within the past year, they have the option, at GitLab's discretion, to purchase their laptop for current market value from GitLab. If the laptop has been used by the team member for more than one year they can, at GitLab's discretion, opt to keep their laptop at no cost, subject to promptly working with the IT Analyst team during the offboarding process to wipe the laptop and have it removed from management system, or, when required by applicable legal obligations, backed up for preservation purposes.

Note that the option to purchase a laptop or to keep a laptop at no cost may be voided where the team member is involved in cases of investigation, misconduct, termination for cause, any violation of [GitLab's Code of Business Conduct & Ethics](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d), or other legal or security related inquiries.

If the team member would like to purchase the laptop at the current market value, they will need to send an email to laptops@gitlab.com to start the process. If purchasing, our Manager of IT, or Lead IT Analyst will approve, and we will send the team member an email with the determined value. If the team member decides to move forward with purchasing, our accounting department will reach out with payment information.

Retained laptops must follow the [wipe process](/handbook/security/corporate/services/laptops/wipe).

If the departing team member opts not to keep or [donate](/handbook/security/corporate/services/laptops/donation) their laptop, they can [return](/handbook/security/corporate/services/laptops/recycle) it to GitLab.
