---
title: "Customer Events"
---

**Event Support Process**

The Customer Advocacy team provides event support to select events as resources allow in alignment with quarterly plans.

To request a customer to speak and/or otherwise support an event please open the [Customer Speaker Request template](https://gitlab.com/gitlab-com/marketing/product-marketing/-/issues/new?issuable_template=customer-speaker-request) a minimum of 90 days before the event. 

For events, please align with Nicole Smith, Director of Customer Advocacy during the quarterly OKR planning and include the Customer Advocacy team in the event kickoff. 

Please review our GitLab Customer Advocacy Event Support Process [here](https://docs.google.com/presentation/d/1LJ1V51Psq6fmhen7FLRGVbzc5n7y4CUrtaYGZazH-uA/edit#slide=id.g2cfaeb0ced0_0_455).
