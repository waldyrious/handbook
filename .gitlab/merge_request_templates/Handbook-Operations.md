<!-- Before proceeding, please check if you need to apply a specific MR description template from the dropdown menu above next to `Description` (e.g. blog post, website bug report). -->

## Why is this change being made?

%{all_commits}

## Author and Reviewer Checklist

**Please verify the check list and ensure to tick them off before the MR is merged.**

- [ ] Confirmed that this update should be in the internal handbook rather than public handbook, per the [SAFE framework](safe-framework)
- [ ] Provided a concise title for this [Merge Request (MR)][mr]
- [ ] Added a description to this MR explaining the reasons for the proposed change, per [**say why, not just what**][say-why-not-just-what]
  - Copy/paste the Slack conversation to document it for later, or upload screenshots. Verify that no confidential data is added, and the content is [SAFE][SAFE]
- [ ] Assign reviewers for this MR to the correct [Directly Responsible Individual/s (DRI)][dri]
    - If the DRI for the page/s being updated isn’t immediately clear, then assign it to one of the people listed in the `Maintained by` section on the page being edited
    - If your manager does not have merge rights, please ask someone to merge it **AFTER** it has been approved by your manager in [#mr-buddies][mr-buddies-slack]
    - The [when to get approval][when-to-get-approval] handbook section explains the workflow in more detail
- [ ] For [transparency][transparency], share this MR with the audience that will be impacted.
   - [ ] Team: For changes that affect your direct team, share in your group Slack channel
   - [ ] Department: If the update affects your department, share the MR in your department Slack channel
   - [ ] Company: If the update affects all (or the majority of) GitLab team members, post an update in [#whats-happening-at-gitlab][whats-happening-at-gitlab-slack] linking to this MR
      - For high-priority company-wide announcements work with the [internal communications][internal-communications] team to post the update in [#company-fyi][company-fyi-slack] and align on a plan to circulate in additional channels like the ["While You Were Iterating" Newsletter][engagement-channels]

<!-- Quick actions for assignment, labels, review requests. Please update them as needed. -->

<!-- Assign yourself -->

/assign me
/assign_reviewer me

<!-- Assign reviewer(s), following https://handbook.gitlab.com/handbook/handbook-usage/#when-to-get-approval. Remove the [HTML comment tags](https://www.w3schools.com/tags/tag_comment.asp) to enable. -->

<!--
/assign_reviewer @
-->

<!-- Apply labels: You can add, keep, or remove labels as needed. -->

/label ~"Handbook::Operations"

---

<!-- DO NOT REMOVE -->
[transparency]: https://handbook.gitlab.com/handbook/values/#transparency
[mr]: https://docs.gitlab.com/ee/user/project/merge_requests/
[say-why-not-just-what]: https://handbook.gitlab.com/handbook/values/#say-why-not-just-what
[dri]: https://handbook.gitlab.com/handbook/people-group/directly-responsible-individuals/
[SAFE]: https://handbook.gitlab.com/handbook/legal/safe-framework/
[when-to-get-approval]: https://handbook.gitlab.com/handbook/handbook-usage/#when-to-get-approval
[internal-communications]: https://handbook.gitlab.com/handbook/people-group/employment-branding/people-communications/
[mr-buddies-slack]: https://gitlab.slack.com/archives/CLM8K5LF4
[company-fyi-slack]: https://gitlab.slack.com/archives/C010XFJFTHN
[whats-happening-at-gitlab-slack]: https://gitlab.slack.com/archives/C0259241C
[engagement-channels]: https://handbook.gitlab.com/handbook/people-group/employment-branding/people-communications/#people-communications--engagement-channels

